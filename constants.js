var path = require('path');

const Separator = "NewMessageSeparator";
const FILE_DIR = 'files';
const MESSAGE_FILE = path.join(FILE_DIR, 'messages.txt');
const PLIVO_NUMBER_FILE = path.join(FILE_DIR, 'plivo-numbers.txt');
const EMAILS_FILE = path.join(FILE_DIR, 'emails.txt');
const GVOICE_NUMBER_FILE = path.join(FILE_DIR, 'gvoice.csv');
const NUMBERS_FILE = path.join(FILE_DIR, 'numbers.txt');
const LOGIN_GMAIL_FILE = path.join(FILE_DIR, 'emails-auth.txt');
const DOT_GMAIL_FILE = path.join(FILE_DIR, 'emails.txt');
const MESSAGE_ARCHIVE_FILE = path.join(FILE_DIR, 'archive.txt');
const NIKE_ACCOUNT_FILE = path.join(FILE_DIR, 'nike.csv');

module.exports = {
    Separator: Separator,
    FILE_DIR: FILE_DIR,
    PLIVO_NUMBER_FILE: PLIVO_NUMBER_FILE,
    EMAILS_FILE: EMAILS_FILE,
    GVOICE_NUMBER_FILE: GVOICE_NUMBER_FILE,
    NUMBERS_FILE: NUMBERS_FILE,
    LOGIN_GMAIL_FILE: LOGIN_GMAIL_FILE,
    DOT_GMAIL_FILE: DOT_GMAIL_FILE,
    MESSAGE_ARCHIVE_FILE: MESSAGE_ARCHIVE_FILE,
    NIKE_ACCOUNT_FILE: NIKE_ACCOUNT_FILE
}