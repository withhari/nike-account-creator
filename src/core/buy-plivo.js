

var nightMare = require('./nmi');
var utils = require('./utils')

var loginUrl = 'https://manage.plivo.com/accounts/login/';
var buyUrl = 'https://manage.plivo.com/number/search/';
var auth = 'rzeiser14@gmail.com:zeiser10'.split(':');
var number;

if (auth.length > 0) {
  nightMare
    .useragent('Mozilla/5.0 (Windows NT 10.0; WOW64; rv:54.0) Gecko/20100101 Firefox/54.0')
    .goto(loginUrl)
    .wait('#id_username')
    .type('#id_username', auth[0])
    .type('#id_password', auth[1])
    .click('#login-sub')
    .wait('#dashnav')
    .then(function () {
      buyNumber();
    })
    .catch((e) => console.log(e));
}

function buyNumber() {
  nightMare
    .goto(buyUrl)
    .wait('#country_chosen')
    .click('#fixed-selector')
    .click('#nf-sms-selector')
    .click('#number-search')
    .wait('table')
    .evaluate(function () {
      var btns = document.querySelectorAll('.buy-btn');
      for (var i = 0; i < btns.length; i++) {
        if (btns[i].innerText.toUpperCase() == 'BUY') {
          btns[i].click();
          break;
        }
      }
      return true;
    })
    .wait('#number-to-rent')
    .click('.buy-number-now')
    .evaluate(function() {
      return document.querySelector('#number-to-rent').innerText;
    })
    .then(function (numb) {
      number = numb;
      console.log('Done:' + numb);
    })
    .catch(function (eror) {
      if (eror)
        console.log(eror);
        buyNumber();
    });
}
