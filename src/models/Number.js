const mongoose = require('mongoose');

const Type = {
    GOOGLE_VOICE: 0,
    PLIVO: 1
};

const NumberSchema = mongoose.Schema({
    content: { type: String },
    used: { type: Number, default: 0 },
    type: { type: Number, default: Type.GOOGLE_VOICE }
});

const NumberModel = mongoose.model('Number', NumberSchema);

function getNumber(type, callback) {
    const GOOGLE_LIMIT = 10;
    const NIKE_LIMIT = 3;
    NumberModel.findOneAndUpdate({
        
    }, {
        used: used + 1
    }, callback);
}

function saveNumber(number, callback, type, tryOnce) {
    if (tryOnce == undefined) {
        tryOnce = true;
    }
    if (type == undefined) {
        type = Type.GOOGLE_VOICE;
    }
    NumberModel.create({
        content: number,
        used: 0,
        type: type
    }).then(res => {
        callback(null, { success: true, number: number });
    }).catch(err => {
        if (err && tryOnce) {
            savePlivoNumber(number, type, callback, false);
        } else {
            callback(err, { success: false, number: number });
        }
    });
}

NumberSchema.statics.getUsablePlivo = function () {
    var number = 1;
    getNumber(Type.PLIVO, function (res) {
        number = res.number;
    });
    while (number === 1) {

    }
    return number;
}

NumberSchema.statics.getUsableGoogle = function () {
    var number = 1;
    getNumber(Type.GOOGLE_VOICE, function (res) {
        number = res.number;
    });
    while (number === 1) {

    }
    return number;
}

module.exports = {
    Number: NumberModel,
    NumberType: Type
};