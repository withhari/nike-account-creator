const mongoose = require('mongoose');

const MessageSchema = new mongoose.Schema({
    content: { type: String },
    sender: { type: String },
    receiver: { type: String },
    date: { type: Date, default: Date.now() }
});

module.exports = mongoose.model('Message', MessageSchema);