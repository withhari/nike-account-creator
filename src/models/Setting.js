const mongoose = require('mongoose');

var SettingSchema = new mongoose.Schema({
    content: String,
    name: String
});

SettingSchema.statics.updateOrInsert = function (doc, callback) {
    this.findOne({ name: doc.name }, (err, res) => {
        if (err) {
            return callback(err);
        }
        if (res) {
            if (res.content == doc.content) return callback(null);
            res.content = doc.content;
        } else {
            res = new this(doc);
        }
        return res.save(error => callback(error));
    })
}

module.exports = mongoose.model('Setting', SettingSchema);