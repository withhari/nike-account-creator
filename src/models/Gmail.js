const mongoose = require('mongoose');

const GmailSchema = mongoose.Schema({
    email: { type: String },
    pass: { type: String, required: true },
    created_on: { type: Date, required: true, default: Date.now() },
    used: { type: Number, default: 0 }
});

module.exports = mongoose.model('Gmail', GmailSchema);