var Model = require('../models/all');
var fs = require('fs');
var path = require('path');

function savePlivo(req, res) {
  Model.Setting.updateOrInsert({
    name: 'plivo.user',
    content: req.body.email + ':' + req.body.pass
  }, err => res.send((err) ? 'Error Occurred.' : 'Successfully saved [plivo.user].'));
}

function saveSetting(req, res) {
  Model.Setting.updateOrInsert({
    name: 'use.proxy',
    content: req.body.proxyEnabled == 'on'
  }, e => console.log(e || 'Done [use.proxy]'));
  Model.Setting.updateOrInsert({
    name: 'quantity',
    content: req.body.quantity
  }, e => console.log(e || 'Done [quantity]'));
  res.send('Setting update received!');
}

function saveEmails(req, res) {
  fs.writeFile(path.join('files', 'emails.txt'), req.body.emails, function (err) {
    res.send(err ? 'Error occurred while saving mails.':'Mails saved.');
  });
}

function saveProxy(req, res) {
  fs.writeFile(path.join('files', 'proxies.txt'), req.body.proxies, function (err) {
    res.send(err ? 'Error occurred while saving proxies.':'Proxies saved.');
  });
}

function savePersonal(req, res) {
  Model.Setting.updateOrInsert({
    name: 'first.name',
    content: req.body.first_name
  }, e => console.log(e || "Done [first.name]"));
  Model.Setting.updateOrInsert({
    name: 'last.name',
    content: req.body.last_name
  }, e => console.log(e || "Done [last.name]"));
  Model.Setting.updateOrInsert({
    name: 'street',
    content: req.body.street
  }, e => console.log(e || "Done [street]"));
  Model.Setting.updateOrInsert({
    name: 'city',
    content: req.body.city
  }, e => console.log(e || "Done [city]"));
  Model.Setting.updateOrInsert({
    name: 'dob',
    content: req.body.dob
  }, e => console.log(e || "Done [dob]"));
  Model.Setting.updateOrInsert({
    name: 'state',
    content: req.body.state
  }, e => console.log(e || "Done [state]"));
  Model.Setting.updateOrInsert({
    name: 'country',
    content: req.body.country
  }, e => console.log(e || "Done [country]"));
  res.send('Request received');
}

module.exports = {
  savePlivo: savePlivo,
  saveSetting: saveSetting,
  saveProxy: saveProxy,
  savePersonal: savePersonal,
  saveEmails: saveEmails
}